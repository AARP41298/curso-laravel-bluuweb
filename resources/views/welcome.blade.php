@extends('plantilla')

@section('seccion')


    <h1 class="display-4">Notas</h1>

    <form action="{{route('notas_crear')}}" method="POST">
        @csrf

        @if (session('mensaje'))
            <div class="alert alert-success alert-dismissible col-6 fade show" role="alert">
                <strong>{{session('mensaje')}}</strong>
                <button type="button" class="close" data-dismiss="alert">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        @endif

        @error('nombre')
        <div class="alert alert-danger alert-dismissible col-6 fade show" role="alert">
            <strong>Falto poner el nombre</strong>
            <button type="button" class="close" data-dismiss="alert">
                <i class="fas fa-times"></i>
            </button>
        </div>
        @enderror

        @error('descripcion')
        <div class="alert alert-danger alert-dismissible col-6 fade show" role="alert">
            <strong>Falto poner la descripción</strong>
            <button type="button" class="close" data-dismiss="alert">
                <i class="fas fa-times"></i>
            </button>
        </div>
        @enderror

        <input type="text" name="nombre" placeholder="Nombre" value="{{old('nombre')}}" class="form-control mb-2">
        <input type="text" name="descripcion" placeholder="Descripción" value="{{old('descripcion')}}"
               class="form-control mb-2">
        <button class="btn btn-success btn-block" data-toggle="tooltip" data-placement="right" title="Agregar"
                type="submit"><i class="fas fa-user-plus fa-3x"></i></button>

    </form>


    <table class="table">
        <thead>
        <tr>
            <th scope="col">#id</th>
            <th scope="col">Nombre</th>
            <th scope="col">Descripción</th>
            <th scope="col">Acciones</th>
        </tr>
        </thead>
        <tbody>
        @foreach($notas as $nota)
            <tr>
                <th scope="row">{{$nota->id}}</th>
                <td>
                    <a href="{{route('notas_detalle',$nota)}}" data-toggle="tooltip" data-placement="right"
                       title="Ver detalles">
                        {{$nota->nombre}}</a>
                </td>
                <td>{{$nota->descripcion}}</td>
                <td>
                    <div class="container">
                        <div class="row">
                            <a href="{{route('notas_editar',$nota)}}" class="btn btn-warning col"
                               data-toggle="tooltip" data-placement="top" title="Editar"><i
                                    class="far fa-edit"></i></a>
                            <form action="{{route('notas_eliminar',$nota)}}" method="POST" class="col">
                                @method('DELETE')
                                @csrf
                                <div class="row">
                                    <button class="btn btn-danger col" data-toggle="tooltip" data-placement="top"
                                            title="Eliminar"><i class="far fa-trash-alt"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{$notas->links('pagination::bootstrap-4')}}

@endsection
