@extends('plantilla')

@section('seccion')

    <h1>Este es mi equipo de trabajo</h1>
    @foreach($equipo as $miembro)
        <a href="{{route('nosotros',$miembro)}}" class="h4 text-danger">{{$miembro}}</a><br>
    @endforeach

    @if(!empty($nombre))
        <h2>Haz seleccionado a {{$nombre}}</h2>
        <p>{{$nombre}} ipsum dolor sit amet, consectetur adipisicing elit. Assumenda autem deleniti libero molestiae quos! Amet aperiam deleniti dicta dolor hic minima nemo, omnis porro quod sequi similique totam velit voluptatem.</p>
    @endif

@endsection
