<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Bootstrap CSS
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
          -->

    <title>Hello, world!</title>
</head>
<body>

<div class="container my-2">
    <a href="{{route('inicio')}}" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom"
       title="Inicio"><i class="fas fa-home fa-3x"></i></a>
    <a href="{{route('fotos')}}" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Fotos"><i
            class="fas fa-photo-video fa-3x"></i></a>
    <a href="{{route('blog')}}" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Blog"><i
            class="fas fa-book fa-3x"></i></a>
    <a href="{{route('nosotros')}}" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom"
       title="Nosotros"><i class="fas fa-user-friends fa-3x"></i></a>
</div>
<div class="container">
    @yield('seccion')
</div>

<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: Bootstrap Bundle with Popper -->

<!-- importe bootstrap con instalacion npm-->
<script src="{{ asset('js/app.js') }}"></script>

<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>

@yield("script")
<!--
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
        crossorigin="anonymous">
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })

</script>
-->


<script src="https://kit.fontawesome.com/f5871d0bdf.js" crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
-->
</body>
</html>
