@extends('plantilla')

@section('seccion')
    <h1>Editar nota {{$nota->id}}</h1>

    <form action="{{route('notas_update',$nota->id)}}" method="POST">
        @method('PUT')
        @csrf

        @if (session('mensaje'))
            <div class="alert alert-success alert-dismissible col-6 fade show" role="alert">
                <strong>{{session('mensaje')}}</strong>
                <button type="button" class="close" data-dismiss="alert">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        @endif

        @error('nombre')
        <div class="alert alert-danger alert-dismissible col-6 fade show" role="alert">
            <strong>Falto poner el nombre</strong>
            <button type="button" class="close" data-dismiss="alert">
                <i class="fas fa-times"></i>
            </button>
        </div>
        @enderror

        @error('descripcion')
        <div class="alert alert-danger alert-dismissible col-6 fade show" role="alert">
            <strong>Falto poner la descripcion</strong>
            <button type="button" class="close" data-dismiss="alert">
                <i class="fas fa-times"></i>
            </button>
        </div>
        @enderror
        <div class="input-group">
            <input type="text" name="nombre" placeholder="Nombre" value="{{$nota->nombre}}"
                   class="form-control col mb-2">
            <div class="input-group-append">
                <button class="btn btn-danger mb-2" type="button" id="clear1" data-toggle="tooltip"
                        data-placement="right" title="Limpiar"><i class="fas fa-times"></i></button>
            </div>
        </div>


        <div class="input-group">
            <input type="text" name="descripcion" placeholder="Descripción"
                   value="{{$nota->descripcion}}"
                   class="form-control col mb-2">
            <div class="input-group-append">
                <button class="btn btn-danger mb-2" type="button" id="clear2" data-toggle="tooltip"
                        data-placement="right" title="Limpiar"><i class="fas fa-times"></i></button>
            </div>
        </div>

        <button class="btn btn-success" type="submit" name="gys" value="false" data-toggle="tooltip"
                data-placement="bottom" title="Guardar"><i class="fas fa-save fa-3x"></i>
        </button>
        <button class="btn btn-warning" type="submit" name="gys" value="true"
                data-toggle="tooltip" data-placement="bottom" title="Guardar y Salir">
            <i class="fas fa-save fa-3x"></i>
            <i class="fas fa-sign-out-alt fa-3x"></i>
        </button>
    </form>




@endsection

@section("script")
    <script>
        /*
        $("#clear1").click(function () {
            $("#nombre").val('');
        });
        */
        document.getElementById("clear1").onclick = function () {
            document.getElementsByName("nombre")[0].value = "";
        }
        document.getElementById("clear2").onclick = function () {
            document.getElementsByName("descripcion")[0].value = "";
        }
    </script>


@endsection
