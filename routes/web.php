<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [\App\Http\Controllers\PagesController::class, 'inicio'])->name('inicio');
Route::get('/notas-detalle/{id}', [\App\Http\Controllers\PagesController::class, 'notas_detalle'])->name('notas_detalle');
Route::get('/notas-editar/{id}', [\App\Http\Controllers\PagesController::class, 'notas_editar'])->name('notas_editar');
Route::put('/notas-update/{id}', [\App\Http\Controllers\PagesController::class, 'notas_update'])->name('notas_update');
Route::delete('/notas-eliminar/{id}', [\App\Http\Controllers\PagesController::class, 'notas_eliminar'])->name('notas_eliminar');


Route::post('/', [\App\Http\Controllers\PagesController::class, 'notas_crear'])->name('notas_crear');
//El name va a ser la referencia que se va a usar para mandar a llamar el href de las plantillas
//El class va a la funcion que hay en el controlador
//el uri es para acceder desde el navegador
Route::get('/fotos', [\App\Http\Controllers\PagesController::class, 'fotos'])->name('fotos');

Route::get('/blog', [\App\Http\Controllers\PagesController::class, 'blog'])->name('blog');

Route::get('nosotros/{nombre?}', [\App\Http\Controllers\PagesController::class, 'nosotros'])->name('nosotros');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
