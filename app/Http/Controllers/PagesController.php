<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Nota;

class PagesController extends Controller
{
    public function inicio()
    {
        $notas = Nota::paginate(3);
        return view('welcome', compact('notas'));
    }

    public function notas_detalle($id)
    {
        $nota = Nota::findOrFail($id);
        return view('notas.detalle', compact('nota'));
    }

    public function notas_crear(Request $request)
    {
        //return $request->all();

        $request->validate([
            'nombre' => 'required',
            'descripcion' => 'required'
        ]);

        $notaNueva = new Nota;
        $notaNueva->nombre = $request->nombre;
        $notaNueva->descripcion = $request->descripcion;

        $notaNueva->save();

        return back()->with('mensaje', 'Nota Agregada');
    }

    public function notas_editar($id)
    {


        $nota = Nota::findOrFail($id);
        return view('notas.editar', compact('nota'));
    }

    public function notas_update(Request $request, $id)
    {
        //return $request->all();
        $request->validate([
            'nombre' => 'required',
            'descripcion' => 'required'
        ]);

        $notaUp = Nota::findOrFail($id);
        $notaUp->nombre = $request->nombre;
        $notaUp->descripcion = $request->descripcion;

        $notaUp->save();

        if ($request->gys == 'true') {
            return redirect('/')->with('mensaje', 'Nota editada');
        } else {
            return back()->with('mensaje', 'Nota editada');
        }

    }

    public function notas_eliminar($id)
    {
        $notaDel = Nota::findOrFail($id);
        $notaDel->delete();
        return back()->with('mensaje', 'Nota eliminada');
    }

    public function fotos()
    {
        return view('fotos');
    }

    public function blog()
    {
        return view('blog');
    }

    public function nosotros($nombre = null)
    {
        $equipo = ['Ignacio', 'Juanito', 'Pedrito'];
        //return view('nosotros',['equipo'=>$equipo]);
        return view('nosotros', compact('equipo', 'nombre'));
    }
}
